﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hoteldeville : gameUnit {

	public GameObject spawn;
    public int buildingDestroy = 0;
	public float spawnTime;
    public string teamEnemie;
    public float timer;
    public Vector3 spawnPosition;
	void Start () {
        timer = spawnTime;
	}
	
	void Update () {
        if (this.HP <= 0)
        {
            Debug.Log("The " + teamEnemie + " Team wins.");
        }
        if (timer >= spawnTime + 2.5*buildingDestroy) {
			timer = 0;
			Instantiate (spawn);
		}
		timer += Time.deltaTime;
        base.Update();
	}

    public void destroyBuilding()
    {
        buildingDestroy++;
    }
}
