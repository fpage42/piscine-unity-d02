﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cursor : MonoBehaviour
{

    List<Character> selected = new List<Character>();
    AudioSource audio;
    public List<AudioClip> startMoovingSounds;
    public AudioClip attackSound;

    void Start()
    {
        audio = GetComponent<AudioSource>();
    }

    void Update()
    {
		if (Input.GetMouseButtonDown (0)) {
			RaycastHit2D hit = Physics2D.Raycast (Camera.main.ScreenToWorldPoint (Input.mousePosition), Vector2.zero);
			if (hit && hit.collider.gameObject.GetComponent<gameUnit> () != null) {
				gameUnit unit = hit.collider.gameObject.GetComponent(typeof(gameUnit)) as gameUnit;
				unit.ClickHit ();
			}
			}
	}

    public void addCharacter(Character c)
    {
        selected.Add(c);
    }

    public void clear()
    {
        selected.Clear();
    }

    public bool isEmpty()
    {
        return (this.selected.Count == 0 ? true : false);
    }

    public void setDestination(Vector2 dest)
    {
        int rand = Random.Range(0, startMoovingSounds.Count);
        if (selected.Count != 0)
            audio.PlayOneShot(startMoovingSounds[rand]);
        foreach (Character c in selected)
        {
            c.goTo(dest);
        }
        setTarget(null);
    }

    public void setTarget(gameUnit target)
    {
		if (target != null)
	        audio.PlayOneShot(attackSound);
        foreach (Character c in selected)
        {
            c.setTarget(target);
        }
    }
}
