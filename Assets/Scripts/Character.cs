﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour {

	private Animator animator;
	public Cursor c;
  
	public enum HeroState {
		STOP,
		MOVE,
        ATTACK
	};

	public float speed;
    public float rangeAttack;
    public int damageAttack;
	private HeroState state = HeroState.STOP;
	private Vector3 destination;
    private gameUnit target;
  
    void Start () {
		c = GameObject.Find ("Cursor").gameObject.GetComponent<Cursor>();
		destination = new Vector2 (transform.position.x, transform.position.y);
		animator = GetComponent<Animator>();
		animator.SetFloat ("destX", destination.x);
		animator.SetFloat ("destY", destination.y);
        this.goTo(new Vector2(-6.3F, -2.66F));
    }
		
	void Update () {
		if (target != null && target.HP <= 0)
			target = null;
        if (target != null)
            this.destination = this.target.transform.position;
        transform.position = (Vector3.MoveTowards(transform.position, destination, speed));
        if (this.target != null && Vector2.Distance(transform.position, this.target.transform.position) <= this.rangeAttack)
        {
            this.target.takeDamage(this.damageAttack);
            this.state = HeroState.ATTACK;
        }
        else if (Vector2.Distance(transform.position, destination) <= speed)
        {
            this.state = HeroState.STOP;
        }
        else
        {
            animator.SetFloat("destX", destination.x - transform.position.x);
            animator.SetFloat("destY", destination.y - transform.position.y);
            this.state = HeroState.MOVE;
        }
        animator.SetInteger("State", (int)state);
    }

    public void goTo(Vector2 destination)
	{
		this.destination = new Vector3 (destination.x, destination.y, -3);
    }

    public void setTarget(gameUnit target)
    {
        this.target = target;
    }

	void OnMouseDown()
	{
		if (Input.GetKey (KeyCode.LeftControl) || Input.GetKey (KeyCode.RightControl) || c.isEmpty ()) {
			c.addCharacter (this);
		}
		else {
			c.clear ();
			c.addCharacter (this);
		}

	}
}
