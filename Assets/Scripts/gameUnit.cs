﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameUnit : MonoBehaviour {

	private enum unitState
	{
		ALIVE,
		DEAD
	};

    public int maxHP;
    public bool canBeTarget;
    public Cursor c;
    public int HP;
	public string unitName;
	AudioSource audio;
	public AudioClip deadSound;
	private unitState state = unitState.ALIVE;
	private Animator anim;

    protected void Start () {
		c = GameObject.Find("Cursor").gameObject.GetComponent<Cursor>();
		anim = gameObject.GetComponent<Animator>();
		this.HP = this.maxHP;
		audio = GetComponent<AudioSource> ();
	}
	
	protected void Update () {
		if (this.HP <= 0) {
			if (audio != null && deadSound != null) {
				this.audio.PlayOneShot (deadSound);
				this.deadSound = null;
				anim.SetInteger ("State", (int)unitState.DEAD);
			}
			if (audio == null || !audio.isPlaying)
				Destroy (gameObject);
		}
	}

    public void takeDamage(int damage)
    {
        this.HP -= damage;
		Debug.Log (this.unitName + "Orc Unit [" + this.HP +"/" + this.maxHP + "]HP has been attacked.");
    }

    public void ClickHit()
    {
        if (canBeTarget)
            c.setTarget(this);
    }
}
